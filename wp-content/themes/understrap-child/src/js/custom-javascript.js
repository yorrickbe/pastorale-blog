// Add your custom JS here.

jQuery(document).ready(function () {
    var trigger = jQuery('.navigation'),
        isClosed = true;

    function buttonSwitch() {
        if (isClosed === true) {
            // overlay.show();
            jQuery('.menu').toggleClass('toggled');
            jQuery('.navigation').toggleClass('toggled').toggleClass('is-active');
            isClosed = false;
            trigger.blur();
        } else {
            // overlay.hide();
            jQuery('.menu').toggleClass('toggled');
            jQuery('.navigation').toggleClass('toggled').toggleClass('is-active');
            isClosed = true;
            trigger.blur();
        }
    }

    trigger.click(function () {
        buttonSwitch();
    });
});
