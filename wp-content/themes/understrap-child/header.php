<?php
/**
 * The header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php //understrap_body_attributes(); ?>>
<?php do_action( 'wp_body_open' ); ?>
<div class="site" id="page">

    <div class="container-fluid">
        <div class="social">
            <div class="facebook">
                <a href="https://www.facebook.com/pastorale/" title="Pastorale - Facebook" target="_blank"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a>
            </div>
            <div class="instagram">
                <a href="https://www.instagram.com/depastorale/" title="Pastorale - Instagram" target="_blank"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a>
            </div>
        </div>

        <button class="hamburger hamburger--collapse navigation is-closed" type="button" data-toggle="offcanvas">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
        </button>

        <nav class="navbar menu" role="navigation">
            <div class="language">
                <ul class="list-inline list-unstyled">
                    <!--                <li class="{{ localization()->getCurrentLocale() == $key ? 'active' : '' }}">-->
                    <!--                    <a href="{{ localization()->getLocalizedURL($key) }}" rel="alternate" hreflang="{{ $key  }}">-->
                    <!--                        {{ $locale->key() }}-->
                    <!--                    </a>-->
                    <!--                </li>-->
                </ul>
            </div>

            <div class="line-small"></div>

            <ul class="nav sidebar-nav">
                <li><a href="https://depastorale.be/pastorale">Pastorale</a></li>
                <li><a href="https://depastorale.be/menu">Menu</a></li>
                <li><a href="https://depastorale.be/reservation">Reservatie</a></li>
                <li><a href="https://depastorale.be/shop">Webshop</a></li>
                <li><a href="https://depastorale.be/events">Events</a></li>
                <li><a href="https://depastorale.be/contact/opening-hours">Praktisch</a></li>
                <li><a href="https://depastorale.be/discover-more">Ontdek Meer</a></li>
                <li><a href="https://depastorale.be/bart-de-pooter">Bart De Pooter</a></li>
                <li class="active"><a href="<?php echo get_home_url(); ?>">Blog</a></li>
                <li><a href="https://depastorale.be/take-away" class="highlight">TakeAway</a></li>
            </ul>

            <div class="line-small"></div>

            <div class="jobs">
                <ul class="list-inline list-unstyled">
                    <li><a href="https://depastorale.be/jobs">Vacature</a></li>
                </ul>
            </div>
        </nav>



        <div class="row">
            <div class="col-md-12 padding-100">
                <h2 class="logo-full">
                    <img class="img-responsive" src="<?php bloginfo('stylesheet_directory'); ?>/img/pastorale-full.png" alt="De Pastorale">
                </h2>

                <h1 class="blog-title text-center"><a href="<?php echo get_home_url(); ?>" class="blog-title">Blog</a></h1>
            </div>
        </div>
    </div>


	<!-- ******************* The Navbar Area ******************* -->
<!--	<div id="wrapper-navbar">-->
<!---->
<!--		<a class="skip-link sr-only sr-only-focusable" href="#content">--><?php //esc_html_e( 'Skip to content', 'understrap' ); ?><!--</a>-->
<!---->
<!--		<nav id="main-nav" class="navbar navbar-expand-md navbar-dark bg-primary" aria-labelledby="main-nav-label">-->
<!---->
<!--			<h2 id="main-nav-label" class="sr-only">-->
<!--				--><?php //esc_html_e( 'Main Navigation', 'understrap' ); ?>
<!--			</h2>-->
<!---->
<!--		--><?php //if ( 'container' === $container ) : ?>
<!--			<div class="container">-->
<!--		--><?php //endif; ?>
<!--                -->
<!--					--><?php //if ( ! has_custom_logo() ) { ?>
<!---->
<!--						--><?php //if ( is_front_page() && is_home() ) : ?>
<!---->
<!--							<h1 class="navbar-brand mb-0"><a rel="home" href="--><?php //echo esc_url( home_url( '/' ) ); ?><!--" itemprop="url">--><?php //bloginfo( 'name' ); ?><!--</a></h1>-->
<!---->
<!--						--><?php //else : ?>
<!---->
<!--							<a class="navbar-brand" rel="home" href="--><?php //echo esc_url( home_url( '/' ) ); ?><!--" itemprop="url">--><?php //bloginfo( 'name' ); ?><!--</a>-->
<!---->
<!--						--><?php //endif; ?>
<!---->
<!--						--><?php
//					} else {
//						the_custom_logo();
//					}
//					?>
<!---->
<!--				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="--><?php //esc_attr_e( 'Toggle navigation', 'understrap' ); ?><!--">-->
<!--					<span class="navbar-toggler-icon"></span>-->
<!--				</button>-->
<!--                -->
<!--				--><?php
//				wp_nav_menu(
//					array(
//						'theme_location'  => 'primary',
//						'container_class' => 'collapse navbar-collapse',
//						'container_id'    => 'navbarNavDropdown',
//						'menu_class'      => 'navbar-nav ml-auto',
//						'fallback_cb'     => '',
//						'menu_id'         => 'main-menu',
//						'depth'           => 2,
//						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
//					)
//				);
//				?>
<!--			--><?php //if ( 'container' === $container ) : ?>
<!--			</div>-->
<!--			--><?php //endif; ?>
<!---->
<!--		</nav>-->
<!---->
<!--	</div>-->
